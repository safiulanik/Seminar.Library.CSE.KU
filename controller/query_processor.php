<?php 
	include("db.php");

	function getRowNo()
	{
		$query="select count(distinct title) as rowNo
				from tblBookInfo
				where title!=''";
		
		$res_count_row = execute_custom_query($query);
		$exeRowNo=sqlsrv_fetch_array($res_count_row);
		$rowNo=$exeRowNo['rowNo'];

		return $rowNo;
	}

	function getAllBooks()
	{
		//select accessNo, distinct(title),author1,author2,author3,author4,publication,edition,self from tblBookInfo
		$query="select distinct title
				from tblBookInfo
				where title!=''";
		
		$res_titles = execute_custom_query($query);

		return $res_titles;
	}

	function getSingleBook($title)
	{
		$query="select accessNo, title,author1,author2,author3,author4,publication,edition,self 
				from tblBookInfo
				where title='".$title."'";

		$exe_single_book = execute_custom_query($query);
		return sqlsrv_fetch_array($exe_single_book);
	}

	function getAllAuthors()
	{
		$query="select distinct author1 as authors
				from tblBookInfo
				where author1!=''
				order by author1";
		$res_authors = execute_custom_query($query);
		return $res_authors;
	}

	function getAllBooksByAuthor($author_name)
	{
		$query="select distinct title
				from tblBookInfo
				where author1='$author_name' or author2='$author_name' or author3='$author_name' or author4='$author_name'";
		$res_authors = execute_custom_query($query);
		return $res_authors;
	}

	function getAllPublishers()
	{
		$query="select distinct publication
				from tblBookInfo
				where publication!=''
				order by publication";
		$res_authors = execute_custom_query($query);
		return $res_authors;
	}

	function getAllBooksByPublisher($publisher_name)
	{
		$query="select distinct title
				from tblBookInfo
				where publication='$publisher_name'";
		$res_publishers = execute_custom_query($query);
		return $res_publishers;
	}

	function getAllMembersWithBooksIssued()
	{
		$query="select distinct memberID
				from tblIssueBook
				where BookStatus='Not Received'";
		$res_members_with_books = execute_custom_query($query);
		return $res_members_with_books;
	}

	function getTotalFine($memberID)
	{
		$query="select ID
				from tblIssueBook
				where memberID='".$memberID."'";
		$res_IDs_of_fined_members = execute_custom_query($query);

		$fine = 0;
		$fine_multiplier = 2;
		while ($res = sqlsrv_fetch_array($res_IDs_of_fined_members)) {
			$query="select ReturnDate
				from tblIssueBook
				where ID='".$res['ID']."'";
			//return $res_IDs_of_fined_members['ID'];
			$res_exec_return_date = execute_custom_query($query);
			$res_return_date = sqlsrv_fetch_array($res_exec_return_date);
			$return_date=$res_return_date['ReturnDate'];
			//return $return_date;
			$query="select DATEDIFF(day,'".date_format($return_date, 'Y-m-d')."','".date("Y-m-d")."') AS DiffDate";
			$res_days = execute_custom_query($query);
			$days = sqlsrv_fetch_array($res_days);
			$days = $days['DiffDate'];
			if($days>0)
			{
				$fine+=($days*$fine_multiplier);
			}
		}
		return $fine;
	}
 ?>