<?php 

    $conn = connectServer();
    
    function closeConnection($conn)
    {
        sqlsrv_close($conn);
    }

    function connectServer()
    {
        $serverName = "safiulanik\SQLExpress";
        $connectionInfo = array("Database"=>"kucse_seminar");

        $conn = sqlsrv_connect($serverName, $connectionInfo);
        if($conn)
        {
            //echo "Connected!<br />";
        }
        else
        {
            echo "Failed to connect to DB :( <br />";
            die(print_r(sqlsrv_errors(),TRUE));
        }
        return $conn;
    }

    function execute_custom_query($query)
    {
        global $conn;
        
        $exe_q1=sqlsrv_query($conn,$query);
        if($exe_q1==false)
        {
            die(print_r(sqlsrv_errors(),true));
        }
        return $exe_q1;
    }
 ?>