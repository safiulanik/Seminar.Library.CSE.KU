
GO
WITH OrderedOrders AS
(
    SELECT *, ROW_NUMBER() OVER (ORDER BY (title)) as rows
    FROM tblBookInfo
)
SELECT * 
FROM OrderedOrders 
WHERE rows BETWEEN 100 AND 120;