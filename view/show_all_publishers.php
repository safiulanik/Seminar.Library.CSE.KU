<?php 
	include("includes/header.php");
	include("../controller/db.php");
	include("../controller/query_processor.php");
?>

<div class="mid-section">
	<?php 
		echo '<div class="col-md-6 center">
    			<div class="panel panel-default">
        			<div class="panel-heading">
            			<h4 class="text-center">List of All Publishers</h4>
        			</div>
        			<div class="panel-body text-center">
        				<div class="author-table center">
						  <table class="table table-bordered table-hover table-striped table-nonfluid center">
						    <thead>
						      <tr>
						        <th class="col-md-1">#</th>
						        <th class="col-md-11">Publisher</th>
						      </tr>
						    </thead>
						    <tbody>';
		$res_publishers = getAllPublishers();
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_publishers))
	 	{
 			echo "<tr>
				    <td  class="."col-sm-1".">".$counter++."</td>
				    
			        <td  class="."col-sm-11"."><a href='show_books_by_publisher.php?publisher-name={$res['publication']}'>{$res['publication']}</a></td>
		    	</tr>";
		}

		echo '
			</tbody>
		  </table>
		</div></div>
		</div>
		</div>';
		?>

</div>
<?php include("includes/footer.php"); ?>
