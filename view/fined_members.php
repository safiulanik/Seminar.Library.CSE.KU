<?php 
	include("includes/header.php");
	include("../controller/db.php");
	include("../controller/query_processor.php");
?>

<div class="mid-section">
	<?php 
		echo '<div class="col-md-6 center">
    			<div class="panel panel-default">
        			<div class="panel-heading">
            			<h4 class="text-center">List of Fined Members</h4>
        			</div>
        			<div class="panel-body text-center">
        				<div class="author-table center">
						  <table class="table table-bordered table-hover table-striped table-nonfluid center">
						    <thead>
						      <tr>
						        <th class="col-md-2">#</th>
						        <th class="col-md-5">Member ID</th>
						        <th class="col-md-5">Fined Amount (BDT)</th>
						      </tr>
						    </thead>
						    <tbody>';
		$res_members_with_books = getAllMembersWithBooksIssued();
	 
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_members_with_books))
	 	{
			$memberID = $res['memberID'];
			$fine=getTotalFine($memberID);
			echo "<tr>
			    <td class="."col-md-2".">".$counter++."</td>
		        <td class="."col-md-5".">".$memberID."</td>
		        <td class="."col-md-5".">".$fine."</td>
		    </tr>";
		}

		/*$res_authors = getAllAuthors();
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_authors))
	 	{
 			echo "<tr>
				    <td  class="."col-sm-1".">".$counter++."</td>
			        <td  class="."col-sm-11"."><a href='show_books_by_author.php?author-name={$res['authors']}'>{$res['authors']}</a></td>
		    	</tr>";
		}*/

		echo '
			</tbody>
		  </table>
		</div></div>
		</div>
		</div>';
	?>

</div>
<?php include("includes/footer.php"); ?>
