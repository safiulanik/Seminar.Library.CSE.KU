<?php include("includes/header.php"); ?>
<div class="mid-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 center">
            <form role="form" action="../controller/exec_add_book.php" method="POST">
                <div class="well well-sm text-center">
                    <strong><span class="glyphicon glyphicon-book"></span> Enter Details of New Book:</strong>
                </div>
                <div class="col-lg-6">
                    <!--<div class="well well-sm">
                        <strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong>
                    </div>-->
                    <div class="form-group">
                        <label for="InputName">Title</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter title" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="InputEmail">Edition</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="InputEmailSecond" name="InputEmail" placeholder="Enter publication" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="InputMessage"><span class="glyphicon glyphicon-tags"></span> Tags</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="tags" name="tags" placeholder="Add Tags">
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="InputEmail">Author(s), separated by ";"</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="InputEmailFirst" name="InputEmail" placeholder="Enter author(s)" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="InputEmail">Publication</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="InputEmailSecond" name="InputEmail" placeholder="Enter publication" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="InputMessage"><span class="glyphicon glyphicon-link"></span> Link to book</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="linktobook" name="linktobook" placeholder="Link to book">
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                </div>
            </form>
            </div>
            <!--
            <div class="col-lg-5 col-md-push-1">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <strong><span class="glyphicon glyphicon-ok"></span> Success! Message sent.</strong>
                    </div>
                    <div class="alert alert-danger">
                        <span class="glyphicon glyphicon-remove"></span><strong> Error! Please check all page inputs.</strong>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>