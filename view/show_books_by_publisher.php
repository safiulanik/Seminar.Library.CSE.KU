<?php 
	include("includes/header.php");
	include("../controller/db.php");
	include("../controller/query_processor.php");
?>

<div class="mid-section">
	<?php
		echo '<div class="container">
			  <h2>List of Books by '.$_GET["publisher-name"].':</h2>
			  <table class="table table-bordered table-hover table-striped .table-condensed">
			    <thead>
			      <tr>
			        <th>#</th>
			        <th>Title</th>
			      </tr>
			    </thead>
			    <tbody>';
			    //echo $_GET['publisher-name'];
		$res_books_by_publisher = getAllBooksByPublisher($_GET["publisher-name"]);
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_books_by_publisher))
	 	{
 			echo "<tr>
				    <td>".$counter++."</td>
			        <td><a href='#'>{$res['title']}</a></td>
		    	</tr>";
		    	//show_books_by_author.php?author-name={$res['authors']}
		}

		echo '
			</tbody>
		  </table>
		</div>';
		?>

</div>
<?php include("includes/footer.php"); ?>
