<?php 
	include("includes/header.php");
	include("../controller/db.php");
	include("../controller/query_processor.php");
?>

<div class="mid-section">
	<?php
		echo '<div class="col-md-6 center">
    			<div class="panel panel-default">
        			<div class="panel-heading">
            			<h4 class="text-center">List of Books by '.$_REQUEST['author-name'].':</h4>
        			</div>
        			<div class="panel-body text-center">
        				<div class="author-table center">
						  <table class="table table-bordered table-hover table-striped table-condensed center">
						    <thead>
						      <tr>
						        <th>#</th>
						        <th>Title</th>
						      </tr>
						    </thead>
						    <tbody>';
		$res_books_by_author = getAllBooksByAuthor($_REQUEST['author-name']);
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_books_by_author))
	 	{
 			echo "<tr>
				    <td>".$counter++."</td>
			        <td><a href='#'>{$res['title']}</a></td>
		    	</tr>";
		    	//show_books_by_author.php?author-name={$res['authors']}
		}

		echo '
			</tbody>
		  </table>
		</div>';
		?>

</div>
<?php include("includes/footer.php"); ?>
