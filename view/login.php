<?php session_start(); ?>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">

<?php include("includes/config.php"); ?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="safiul kabir">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title[$page];?></title>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">CSE Seminar Library</a>
            </div>
            
            <div class="collapse navbar-collapse" id="navbar-collapse">
            <?php
                echo '<ul class="nav navbar-nav">
                        <li><a href="show_all_books.php">Show All Books</a></li>
                        <li><a href="show_all_authors.php">Browse by Author</a></li>
                        <li><a href="#">Browse by Publisher</a></li>
                        <li><a href="#">Browse by Tags</a></li>
                    </ul>'
            ?>
            </div>

        </div>
    </nav>

    <div class="mid-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Sign in to your account</h1>
                    <div class="account-wall">
                        <!--<img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                            alt="">-->
                        <form class="form-signin" action="../controller/login_auth.php" method="POST">
                        <input name="username" type="text" class="form-control" placeholder="Username" required autofocus>
                        <input name="password" type="password" class="form-control" placeholder="Password" required>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                            Sign in</button>
                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me">
                            Remember me
                        </label>
                        <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                        </form>
                    </div>
                    <!--<a href="#" class="text-center new-account">Create an account </a>-->
                </div>
            </div>
        </div>
    </div>


    <?php  include("includes/footer.php");?>
