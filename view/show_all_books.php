<?php 
	include("includes/header.php");
	include("../controller/db.php");
	include("../controller/query_processor.php");
?>

<div class="mid-section">
	<?php 
		echo '<div class="col-md-12 center">
    			<div class="panel panel-default">
        			<div class="panel-heading">
            			<h4 class="text-center">List of All Books</h4>
        			</div>
        			<div class="panel-body text-center">
        				<div class=" center">
						  <table class="table table-bordered table-hover table-striped table-nonfluid">
			    <thead>
			      <tr>
			        <th class="col-md-1">#</th>
			        <th class="col-md-4">Title</th>
			        <th class="col-md-3">Author(s)</th>
			        <th class="col-md-1">Edition</th>
			        <th class="col-md-2">Publication</th>
			        <th class="col-md-1">Self</th>
			      </tr>
			    </thead>
			    <tbody>';
		$res_titles = getAllBooks();
	 
	 	$counter=1;
	 	while($res = sqlsrv_fetch_array($res_titles))
	 	{
	 		//echo $res['title'];
	 		if($res['title']!="")
			{
				$res=getSingleBook($res['title']);
				echo "<tr>
				    <td class="."col-md-1".">".$counter++."</td>
			        <td class="."col-md-4".">{$res['title']}</td>
			        <td class="."col-md-3".">{$res['author1']}";
			        if($res['author2']!='')
		        	{
		        		echo ", {$res['author2']}";
		        	}
		        	if($res['author3']!='')
		        	{
		        		echo ", {$res['author3']}";
		        	}
		        	if($res['author4']!='')
		        	{
		        		echo ", {$res['author4']}";
		        	}
		        	echo "</td>
			        <td class="."col-md-1".">{$res['edition']}</td>
			        <td class="."col-md-2".">{$res['publication']}</td>
			        <td class="."col-md-1".">{$res['self']}</td>
			    </tr>";
			}
		}

		echo '
			</tbody>
		  </table>
		</div></div>
		</div>
		</div>';
		?>

</div>

<?php include("includes/footer.php"); ?>
