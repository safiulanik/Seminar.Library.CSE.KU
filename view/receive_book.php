<?php include("includes/header.php"); ?>
<div class="mid-section">
    <div class="container">
        <div class="row">
            <form role="form" action="../controller/exec_receive_book.php" method="POST">
                <div class="col-lg-4 center">
                    <div class="well well-sm text-center">
                        <strong><span class="glyphicon glyphicon-book"></span> Receive Book</strong>
                    </div>
                    <div class="form-group">
                        <label for="InputName">Book Accession No.</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="InputName" id="InputName" placeholder="accn no." required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InputName">Member ID</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="InputID" id="InputID" placeholder="Enter member ID" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                    <div>
                        <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                    </div>
                </div>
                    
            </form>
            <!--
            <div class="col-lg-5 col-md-push-1">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <strong><span class="glyphicon glyphicon-ok"></span> Success! Message sent.</strong>
                    </div>
                    <div class="alert alert-danger">
                        <span class="glyphicon glyphicon-remove"></span><strong> Error! Please check all page inputs.</strong>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>