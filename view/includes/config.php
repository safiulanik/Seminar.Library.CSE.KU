<?php

	$title = array(
		"index.php"=>"Home :: KUCSE Seminar Library",
		"login.php"=>"Login :: KUCSE Seminar Library",
		"show_all_books.php"=>"All Books :: KUCSE Seminar Library",
		"show_all_authors.php"=>"All Authors :: KUCSE Seminar Library",
		"show_all_publishers.php"=>"All Publishers :: KUCSE Seminar Library",
		"show_books_by_author.php"=>"Books by Author :: KUCSE Seminar Library",
		"add_new_book.php"=>"Add New Book :: KUCSE Seminar Library",
		"add_new_book_confirmation.php"=>"Confirmation :: KUCSE Seminar Library",
		"add_new_member.php"=>"Add New Member :: KUCSE Seminar Library",
		"issue_book.php"=>"Issue Book :: KUCSE Seminar Library",
		"receive_book.php"=>"Receive Book :: KUCSE Seminar Library",
		"fined_members.php"=>"Fined Members :: KUCSE Seminar Library",
		"view_member_history.php"=>"Member History :: KUCSE Seminar Library",
		"edit_member_history.php"=>"Member History :: KUCSE Seminar Library",
		"clearance.php"=>"Clearance :: KUCSE Seminar Library"
	);

	$page = basename(htmlspecialchars($_SERVER['PHP_SELF']));
?>