<!-- GALLERY -->
<section>
	<div class="container-fluid jumbotron" id="section-gallery">
		<h1>Welcome!</h1> <p>Take a look at some popular books.</p>
	</div> <!-- end page-header -->

	<div class="carousel slide" id="gallery-carousel" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#gallery-carousel" data-slide-to="0" class="active"></li>
			<li data-target="#gallery-carousel" data-slide-to="1"></li>
			<li data-target="#gallery-carousel" data-slide-to="2"></li>
		</ol>

		<div class="carousel-inner">
			<div class="item active">
				<img src="view/resources/slider-1 - Copy.jpg" alt="Slider image">
				<div class="carousel-caption">This is a caption.</div>
			</div>
			<div class="item">
				<img src="view/resources/slider-2.jpg" alt="Slider image">
				<div class="carousel-caption">This is a caption.</div>
			</div>
			<div class="item">
				<img src="view/resources/slider-3.jpg" alt="Slider image">
				<div class="carousel-caption">This is a caption.</div>
			</div>
		</div> <!-- end carousel-inner -->

		<a href="#gallery-carousel" class="left carousel-control" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a href="#gallery-carousel" class="right carousel-control" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div> <!-- end carousel -->
</section>