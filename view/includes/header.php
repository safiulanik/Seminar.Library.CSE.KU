<?php session_start(); ?>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">

<?php include("includes/config.php"); ?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="author" content="safiul kabir">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title[$page];?></title>
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	
	<nav class="navbar navbar-inverse navbar-fixed-top">
	  	<div class="container-fluid">
	    	<div class="navbar-header">
	    		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
	      		<a class="navbar-brand" href="index.php">CSE Seminar Library</a>
	    	</div>
	    	<div class="collapse navbar-collapse" id="navbar-collapse">
			
			<?php
				if(isset($_SESSION["manager"]))
				{
					echo '<ul class="nav navbar-nav">
				        	<li class="dropdown">
				        		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Browse Book
				        		<span class="caret"></span></a>
				        		<ul class="dropdown-menu">
				        			<li><a href="show_all_books.php">All Books</a></li>
				        			<li><a href="show_all_authors.php">By Author</a></li>
				        			<li><a href="show_all_publishers.php">By Publication</a></li>
				        			<li><a href="show_all_tags.php">By Tags</a></li>
				        		</ul>
				        	</li>
				        	<li><a href="add_new_book.php">Add Book</a></li>
				        	<li><a href="add_new_member.php">Add Member</a></li>
				        	<li><a href="issue_book.php">Issue Book</a></li>
				        	<li><a href="receive_book.php">Receive Book</a></li>
				        	<li><a href='.'clearance.php?member='.$_SESSION["manager"].'>Clearance</a></li>
				        	<li><a href='.'edit_member_history.php?member='.$_SESSION["manager"].'>View Member History</a></li>
				        	<li><a href="fined_members.php">Fined Members</a></li>
				      	</ul>
				      	<ul class="nav navbar-nav navbar-right">
				        	<li><a href="../controller/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
				      	</ul>';
				}
				else if(isset($_SESSION["member"]))
				{
					echo '<ul class="nav navbar-nav">
				        	<li class="dropdown">
				        		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Browse Book
				        		<span class="caret"></span></a>
				        		<ul class="dropdown-menu">
				        			<li><a href="show_all_books.php">All Books</a></li>
				        			<li><a href="show_all_authors.php">By Author</a></li>
				        			<li><a href="show_all_publishers.php">By Publication</a></li>
				        			<li><a href="show_all_tags.php">By Tags</a></li>
				        		</ul>
				        	</li>
				        	<li><a href='.'currently_issued_books.php?member='.$_SESSION["member"].'>Currently Issed Books</a></li>
				        	<li><a href='.'view_member_history.php?member='.$_SESSION["member"].'>History</a></li>
				      	</ul>
				      	<ul class="nav navbar-nav navbar-right">
				        	<li><a href="../controller/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
				      	</ul>';
				}
				else
				{
					echo '<ul class="nav navbar-nav">
			    			<li><a href="show_all_books.php">Show All Books</a></li>
			    			<li><a href="show_all_authors.php">Browse by Author</a></li>
			    			<li><a href="show_all_publishers.php">Browse by Publisher</a></li>
			    			<li><a href="#">Browse by Tags</a></li>
				      	</ul>
				      	<ul class="nav navbar-nav navbar-right">
				        	<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				      	</ul>';			
				}
			?>
			</div>

		</div>
	</nav>