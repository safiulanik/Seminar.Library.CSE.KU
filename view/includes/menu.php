<nav>
	<div class="container clearfix">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="">Explore</a>
				<ul>
					<li><a href="showallbooks.php">Show all books</a></li>
					<li><a href="show_all_author.php">Browse by author</a></li>
					<li><a href="show_all_category.php">Browse by category</a></li>
					<li><a href="show_all_publisher.php">Browse by publisher</a></li>
				</ul>
			</li>
			<li><a href="usingthelibrary.php">Using the library</a></li>
			<li><a href="research.php">Research</a></li>
			<li><a href="eventsnclasses.php">Events &amp; Classes</a></li>
			<li><a href="schedule.php">Schedule</a></li>
			<li><a href="location.php">Location</a></li>
			<li><a href="about.php">About</a></li>
		</ul>
	</div>
</nav>